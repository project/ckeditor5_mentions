# CKEditor 5 Premium Features

CKEditor 5 Mentions module for Drupal 9/10 that will provides a mention feature
enables smart autocompletion based on user input. When you type a pre-configured
marker, such as @ or #, a panel displays with autocomplete suggestions.

You can type the “@” character to invoke the mention autocomplete UI. 
The demo below is configured to suggest a static list of names 
(“Barney”, “Lily”, “Marry Ann”, “Marshall”, “Robin”, and “Ted”).

## Providing the feed

The feed can be provided as:

 * A static array – Good for scenarios with a relatively small set of
                    autocomplete items.
 * A callback – It provides more control over the returned list of items.

When using a callback you can return a Promise that resolves with the list
of matching feed items. These can be simple strings or plain objects with at
least the name property. The other properties of this object can later be used
e.g. when customizing the autocomplete list or customizing the output.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ckeditor5_mentions).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ckeditor5_mentions).


## CKEditor 5 Mentions Module

The CKEditor 5 Mentions module: [project page](https://www.drupal.org/project/ckeditor5_mentions)

Online documentation: [CKEditor 5 Mentions](https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html)


## Status 🚧

The module is currently under active development. Have questions about the status, roadmap or would you like to use it in your project? Contact us: [ckeditor](https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html/)

Reporting issues: [issue queue](https://www.drupal.org/project/issues/ckeditor5_mentions)


## Installation:

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### Using composer:
```composer require drupal/ckeditor5_mentions```


## Requirements

**PHP** >= 8.1

**Drupal** >= 9.4 || 10


## Configuration

Go to /admin/config/content/mention-feed to add/edit Mention Feeds where we can configure Marker, autocomplete Entity type and other configurations. Go to /admin/config/content/formats, and configure Text editor with CKeditor5 and enable the required feed items from Mentions Configuration. Online documentation: [CKEditor 5 Premium Features](https://www.drupal.org/docs/contributed-modules/ckeditor-5-premium-features)


## Uninstallation

### Using composer:
```composer require remove/ckeditor5_mentions```


## License

Licensed under the terms of GNU General Public License Version 2 or later.
