<?php

namespace Drupal\ckeditor5_mentions;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a mention entity type.
 */
interface MentionInterface extends ContentEntityInterface, EntityOwnerInterface {

}
