<?php

namespace Drupal\ckeditor5_mentions\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Mentions Type form.
 *
 * @property \Drupal\ckeditor5_mentions\MentionFeedInterface $entity
 */
class MentionFeedForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Title for the mention feed.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\ckeditor5_mentions\Entity\MentionFeed::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the mention feed.'),
    ];
    $form['marker'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Marker'),
      '#default_value' => $this->entity->get('marker'),
      '#maxlength' => 1,
      '#required' => TRUE,
      '#description' => $this->t('Marker of the mentions type e.g. <em>@</em>, <em>#</em> etc.'),
    ];
    $form['minimum_characters'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum characters'),
      '#default_value' => $this->entity->get('minimum_characters') ?? 0,
      '#min' => '0',
      '#max' => '10',
      '#description' => $this->t('The minimum count of characters to type before autocompletion.'),
    ];
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#options' => all_entity_types(),
      '#default_value' => $this->entity->get('entity_type'),
      '#ajax' => [
        'callback' => [$this, 'reloadBundle'],
        'event' => 'change',
        'wrapper' => 'entity_bundle-field-wrapper',
      ],
      '#required' => TRUE,
      '#description' => $this->t('Entity type for the smart autocompletion based on user input'),
    ];

    $form['entity_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity bundle'),
      '#default_value' => $this->entity->get('entity_bundle'),
      '#prefix' => '<div id="entity_bundle-field-wrapper">',
      '#suffix' => '</div>',
      '#options' => $this->getBundlesForEntity($this->entity->get('entity_type')),
      '#states' => [
        'visible' => [
          'select[name="entity_type"]' => ['!value' => ''],
        ],
      ],
      '#description' => $this->t('Entity bundle for the smart autocompletion based on user input'),
    ];
    $form['feed_items'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Feed Items'),
      '#default_value' => implode("\r\n", (array) $this->entity->get('feed_items')),
      '#description' => $this->t('Additional feed items, one per line, to be added to the feed e.g., <em>@siteadmin</em>, <em>@demo</em> or <em>#channel</em> same as the current marker.'),
    ];
    $form['feed_callable'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Feed Callable'),
      '#default_value' => $this->entity->get('feed_callable'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function reloadBundle(array $form, FormStateInterface $form_state) {
    return $form['entity_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getBundlesForEntity($entity_type) {
    $get_entity_bundles = get_entity_bundles($entity_type);

    // if(empty($get_entity_bundles) || (!empty($get_entity_bundles)
    // && count($get_entity_bundles) == 1)) {.
    if (empty($get_entity_bundles)) {
      $get_entity_bundles = [
        $entity_type => ($entity_type) ? ucfirst($entity_type) : '',
      ];
    }
    else {
      $all = [
        '' => $this->t('- All -'),
      ];
      $get_entity_bundles = $all + $get_entity_bundles;
    }
    return $get_entity_bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Massage submitted form values.
    $form_state->setValue('feed_items', explode("\r\n", $form_state->getValue('feed_items')));
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
    ? $this->t('Created new mention feed %label.', $message_args)
    : $this->t('Updated mention feed %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $result;
  }

}
