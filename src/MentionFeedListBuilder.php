<?php

namespace Drupal\ckeditor5_mentions;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of mentions types.
 */
class MentionFeedListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Feed Item');
    $header['id'] = $this->t('Machine name');
    $header['marker'] = $this->t('Marker');
    $header['description'] = $this->t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['marker'] = $entity->get('marker');
    $row['description'] = $entity->get('description');
    return $row + parent::buildRow($entity);
  }

}
