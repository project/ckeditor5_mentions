<?php

namespace Drupal\ckeditor5_mentions\Mention;

/**
 * Mention feed item.
 */
class MentionFeedItem implements MentionFeedItemInterface {

  /**
   * The mention id string.
   *
   * @var string
   */
  public string $id;

  /**
   * The constructor.
   *
   * @param string $id
   *   The mention id.
   */
  public function __construct(string $id) {
    $this->id = $id;
  }

}
