<?php

namespace Drupal\ckeditor5_mentions\Mention;

/**
 * Mention feed interface.
 */
interface MentionFeedInterface {

  /**
   * Get feed items.
   *
   * @return \Drupal\ckeditor5_mentions\Mention\MentionFeedItem[]
   *   An array of mention feed item objects.
   */
  public function getFeedItems(string $query = ''):array;

}
