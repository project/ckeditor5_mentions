<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Controller;

use Drupal\ckeditor5_mentions\DataProvider\MentionDataProvider;
use Drupal\ckeditor5_mentions\Entity\MentionFeed;
use Drupal\ckeditor5_mentions\Utility\MentionSettings;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller handling mention requests.
 */
class MentionAutocompleteController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\ckeditor5_mentions\DataProvider\MentionDataProvider $mentionsProvider
   *   User data provider.
   * @param \Drupal\ckeditor5_mentions\Utility\MentionSettings $mentionSettings
   *   Mention settings.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    protected MentionDataProvider $mentionsProvider,
    protected MentionSettings $mentionSettings,
    protected RequestStack $requestStack,
    EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ckeditor5_mentions.data_provider.mentions'),
      $container->get('ckeditor5_mentions.mention_settings'),
      $container->get('request_stack'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Method returning a json response with users matching query criteria.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function annotation() {
    $args = $this->requestStack->getCurrentRequest()->query;

    if (empty($args->get('query'))) {
      return new JsonResponse([]);
    }
    if (empty($args->get('id'))) {
      return new JsonResponse([]);
    }
    $limit = 10;

    if ($args->get('limit')) {
      $limit = $args->get('limit');
    }

    $limit = (int) $limit;

    $resultList = [];
    $mention_feeds = MentionFeed::loadMultiple([$args->get('id')]);

    if ($mention_feeds) {
      foreach ($mention_feeds as $mention_feed) {
        $marker = $mention_feed->get('marker');
        $entity_type = $mention_feed->get('entity_type');
        $entity_bundle = $mention_feed->get('entity_bundle');
        $feed_items = $mention_feed->get('feed_items');
        $search_term = $args->get('query');

        $storage = $this->entityTypeManager->getStorage($entity_type);

        $entity_query = $storage->getQuery()->accessCheck(TRUE);
        $entity_query->range(0, $limit);

        if ($storage->getEntityType()->getKey('published')) {
          $entity_query->condition(
            $storage->getEntityType()->getKey('published'),
            TRUE
          );
        }

        if ($entity_bundle && $storage->getEntityType()->getKey('bundle')) {
          $entity_query->condition(
            $storage->getEntityType()->getKey('bundle'),
            $entity_bundle
          );
        }

        if (!empty($search_term) && $entity_type === 'user') {
          $or = $entity_query->orConditionGroup()
            ->condition('name', $search_term, 'CONTAINS')
            ->condition('mail', $search_term, 'CONTAINS');
          $entity_query->condition($or);
        }
        elseif (!empty($search_term) && $storage->getEntityType()->getKey('label')) {
          $entity_query->condition(
            $storage->getEntityType()->getKey('label'),
            $search_term,
            'CONTAINS'
                  );
        }

        $entity_ids = (array) $entity_query->execute();
        if ($entity_ids) {

          $entities = $storage->loadMultiple($entity_ids);

          if ($entities) {
            foreach ($entities as $entity) {
              if ($entity->id() == 0) {
                continue;
              }
              $resultList[] = [
                'id' => $marker . $entity->label(),
                'link' => '',
              ];
            }
          }
        }
      }
    }

    if (is_array($feed_items)) {
      foreach ($feed_items as $feed_item) {
        if (strpos($feed_item, $marker) === 0) {
          if (preg_match("/" . $search_term . "/", $feed_item)) {
            $resultList[] = [
              'id' => $feed_item,
              'link' => '',
            ];
          }
        }
      }
    }

    return new AjaxResponse($resultList);
  }

  /**
   * Returns a marker character used for starting annotations.
   */
  protected function getMentionMarker(): string {
    return $this->mentionSettings->getMentionsMarker();
  }

  /**
   * Returns the maximum number of suggestions displayed.
   */
  protected function getDropdownLimit(): int {
    return $this->mentionSettings->getMentionAutocompleteListLength();
  }

}
