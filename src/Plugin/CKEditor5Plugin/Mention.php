<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5_mentions\Entity\MentionFeed;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * CKEditor 5 Mention plugin.
 *
 * @internal
 *   Plugin classes are internal.
 */
class Mention extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * The default configuration for this plugin.
   *
   * @var string[][]
   */
  const DEFAULT_CONFIGURATION = [
    'dropdown_limit' => 10,
    'commit_keys' => [13, 32],
    'mention_feeds_enabled' => [],
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return static::DEFAULT_CONFIGURATION;
  }

  /**
   * {@inheritdoc}
   *
   * Form for choosing which heading tags are available.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];
    /** @var \Drupal\ckeditor5_mentions\Entity\MentionFeed[] $mention_feeds */
    $mention_feeds = MentionFeed::loadMultiple();
    foreach ($mention_feeds as $mention_feed) {
      $options[$mention_feed->id()] = $this->t('@label: @description', [
        '@label' => $mention_feed->label(),
        '@description' => $mention_feed->get('description'),
      ]);
    }
    $form['mention_feeds_enabled'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled mention feeds'),
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['mention_feeds_enabled'],
    ];
    if (\Drupal::currentUser()->hasPermission('administer filters')) {
      $form['mention_feeds_enabled']['#description'] = $this->t('You might want to add new or configure existing mention feeds at the <a href="@url">Mention types</a> configuration page.', [
        '@url' => Url::fromRoute('entity.mention_feed.collection')->toString(),
      ]);
    }

    $form['dropdown_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Dropdown limit'),
      '#description' => $this->t('How many items to be shown on the dropdown list.'),
      '#size' => 2,
      '#min' => 5,
      '#max' => 50,
      '#default_value' => $this->configuration['dropdown_limit'],
    ];
    $form['commit_keys'] = [
      '#type' => 'select',
      '#options' => array_combine(static::DEFAULT_CONFIGURATION['commit_keys'], static::DEFAULT_CONFIGURATION['commit_keys']),
      '#multiple' => TRUE,
      '#attributes' => ['hidden' => 'hidden'],
      '#default_value' => $this->configuration['commit_keys'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['dropdown_limit'] = $form_state->getValue('dropdown_limit');
    $this->configuration['mention_feeds_enabled'] = $form_state->getValue('mention_feeds_enabled');
  }

}
