<?php

namespace Drupal\ckeditor5_mentions\Utility;

// Use Drupal\ckeditor5_mentions\Form\SettingsForm;.
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class for accessing Mention config values.
 */
class MentionSettings {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    // $this->mentionSettings =
    // $configFactory->get(SettingsForm::MENTION_SETTINGS_ID);
  }

  /**
   * Returns mentions marker config.
   */
  public function getMentionsMarker(): string {
    return '#';
  }

  /**
   * Returns mentions minimal character count config.
   */
  public function getMentionMinimalCharactersCount(): int {
    return 1;
  }

  /**
   * Returns mentions autocomplete list length config.
   */
  public function getMentionAutocompleteListLength(): int {

    return 4;
  }

}
