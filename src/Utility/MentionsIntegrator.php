<?php

namespace Drupal\ckeditor5_mentions\Utility;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service helping with mentions submodule integration.
 */
class MentionsIntegrator {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * Checks if the Mentions module is enabled.
   */
  public function isMentionInstalled(): bool {
    return $this->moduleHandler->moduleExists('ckeditor5_mentions');
  }

  /**
   * Returns MentionsHelper service if Mentions module is enabled.
   */
  public function getMentionHelperService(): ?MentionsHelper {
    if (!$this->isMentionInstalled()) {
      return NULL;
    }

    return $this->container->get('ckeditor5_mentions.mentions_helper');
  }

}
