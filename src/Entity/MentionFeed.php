<?php

namespace Drupal\ckeditor5_mentions\Entity;

use Drupal\ckeditor5_mentions\MentionFeedInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the mentions type entity type.
 *
 * @ConfigEntityType(
 *   id = "mention_feed",
 *   label = @Translation("Mention Type"),
 *   label_collection = @Translation("Mention Feeds"),
 *   label_singular = @Translation("mention feed"),
 *   label_plural = @Translation("mention feeds"),
 *   label_count = @PluralTranslation(
 *     singular = "@count mention feed",
 *     plural = "@count mention feeds",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\ckeditor5_mentions\MentionFeedListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ckeditor5_mentions\Form\MentionFeedForm",
 *       "edit" = "Drupal\ckeditor5_mentions\Form\MentionFeedForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "mention_feed",
 *   admin_permission = "administer mention_feed",
 *   links = {
 *     "collection" = "/admin/structure/mention-feed",
 *     "add-form" = "/admin/structure/mention-feed/add",
 *     "edit-form" = "/admin/structure/mention-feed/{mention_feed}",
 *     "delete-form" = "/admin/structure/mention-feed/{mention_feed}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "marker",
 *     "minimum_characters",
 *     "entity_type",
 *     "entity_bundle",
 *     "feed_callable",
 *     "feed_items"
 *   }
 * )
 */
class MentionFeed extends ConfigEntityBase implements MentionFeedInterface {

  /**
   * The mention feed ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The mention feed label.
   *
   * @var string
   */
  protected $label;

  /**
   * The mention_feed description.
   *
   * @var string
   */
  protected $description;

  /**
   * The mention_feed marker.
   *
   * @var string
   */
  protected $marker;

  /**
   * The mention_feed minimum characters.
   *
   * @var string
   */
  protected $minimum_characters;

  /**
   * The mention_feed entity type.
   *
   * @var string
   */
  protected $entity_type;

  /**
   * The mention_feed entity bundle.
   *
   * @var string
   */
  protected $entity_bundle;

  /**
   * The mention_feed feed callable.
   *
   * @var string
   */
  protected $feed_callable;

  /**
   * The mention_feed feed items.
   *
   * @var string[]
   */
  protected $feed_items;

}
