<?php

namespace Drupal\ckeditor5_mentions\Entity;

use Drupal\ckeditor5_mentions\MentionInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the mention entity class.
 *
 * @ContentEntityType(
 *   id = "mention",
 *   label = @Translation("Mention"),
 *   label_collection = @Translation("Mentions"),
 *   label_singular = @Translation("mention"),
 *   label_plural = @Translation("mentions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count mentions",
 *     plural = "@count mentions",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\ckeditor5_mentions\MentionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\ckeditor5_mentions\Form\MentionForm",
 *       "edit" = "Drupal\ckeditor5_mentions\Form\MentionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\ckeditor5_mentions\Routing\MentionHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "mention",
 *   admin_permission = "administer mention",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/mention",
 *     "add-form" = "/admin/content/mention/add",
 *     "canonical" = "/admin/content/mention/{mention}",
 *     "edit-form" = "/admin/content/mention/{mention}",
 *     "delete-form" = "/admin/content/mention/{mention}/delete",
 *   },
 * )
 */
class Mention extends ContentEntityBase implements MentionInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the mention was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);
    return $fields;
  }

}
