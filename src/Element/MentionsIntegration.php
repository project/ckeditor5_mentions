<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_mentions\Element;

use Drupal\ckeditor5\EditorManager;
use Drupal\ckeditor5_mentions\Entity\MentionFeed;
use Drupal\ckeditor5_mentions\Utility\MentionSettings;
use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;

/**
 * Defines the Text Format utility class for handling the Mention data.
 */
class MentionsIntegration {

  /**
   * The Editor service.
   *
   * @var \Drupal\ckeditor5_mentions\EditorManger
   */
  protected $editorManager;

  /**
   * Creates the mentions integration element instance.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user.
   * @param \Drupal\ckeditor5_mentions\Utility\MentionSettings $mentionSettings
   *   Mention settings helper.
   * @param \Drupal\ckeditor5_mentions\EditorManager $editorManager
   *   Editor manager service.
   */
  public function __construct(
    protected AccountProxyInterface $currentUser,
    protected MentionSettings $mentionSettings,
    EditorManager $editorManager
  ) {
    $this->editorManager = $editorManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('current_user'),
      $container->get('ckeditor5_mentions.mention_settings'),
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ckeditor5_mentions.editor_manager')
    );
  }

  /**
   * Process the text_format form element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form.
   * @param array $complete_form
   *   The form structure.
   *
   * @return array
   *   The element data.
   */
  public function processElement(array &$element, FormStateInterface $form_state, array &$complete_form): array {

    $form_object = $form_state->getFormObject();
    if (!$form_object instanceof EntityFormInterface || !$form_object->getEntity() instanceof EntityInterface) {
      // Do not process anything, the entity is missing.
      return $element;
    }

    if (!$this->currentUser->hasPermission('mention users')) {
      return $element;
    }

    $mentions = [];

    $editors = $this->editorManager->loadMultiple();

    if ($editors) {
      $active_editors = [];
      foreach ($editors as $editor_key => $editor) {
        $settings = $editor->getSettings();
        if ($settings) {
          $mention_feeds_enabled = $settings['plugins']['ckeditor5_mentions_mention']['mention_feeds_enabled'] ?? '';
          if ($mention_feeds_enabled != '') {
            $active_editors[] = $editor_key;
            $mention_feeds = MentionFeed::loadMultiple($mention_feeds_enabled);

            if ($mention_feeds) {
              foreach ($mention_feeds as $mention_feed) {

                // $default_class =
                // 'Drupal\ckeditor5_mentions\Mention\MentionFeedEntity';
                // $reflection = new \ReflectionClass($default_class);
                // $mention_feed =
                // $reflection->newInstanceArgs([$mention_feed]);
                // $feed_items = $mention_feed->getFeedItems();
                $mentions[$editor_key][] = [
                  'id' => $mention_feed->get('id'),
                  'marker' => $mention_feed->get('marker'),
                  'minimumCharacters' => $mention_feed->get('minimum_characters'),
                ];
              }
            }
          }
        }
      }
    }

    $annotations_url = Url::fromRoute('ckeditor5_mentions.annotations', [], ["absolute" => TRUE])->toString();
    $dropdown_limit = $settings['plugins']['ckeditor5_mentions_mention']['dropdown_limit'] ?? 10;
    $element['#attached']['drupalSettings']['annotations_url'] = $annotations_url;
    $element['#attached']['drupalSettings']['dropdown_limit'] = $dropdown_limit;
    $element['#attached']['drupalSettings']['ckeditor5Premium']['mentions'] = $mentions;
    $element['#attached']['drupalSettings']['ckeditor5Premium']['mention_feeds_enabled'] = $mention_feeds_enabled;
    $element['#attached']['drupalSettings']['ckeditor5Premium']['active_editors'] = $active_editors;

    return $element;
  }

  /**
   * Process the text_format form element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form.
   * @param array $complete_form
   *   The form structure.
   *
   * @return array
   *   The element data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function process(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $service = \Drupal::service('ckeditor5_mentions.element.mentions_integration');
    return $service->processElement($element, $form_state, $complete_form);
  }

}
