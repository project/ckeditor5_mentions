!(function (e, t) {
  "object" == typeof exports && "object" == typeof module
    ? (module.exports = t())
    : "function" == typeof define && define.amd
    ? define([], t)
    : "object" == typeof exports
    ? (exports.CKEditor5 = t())
    : ((e.CKEditor5 = e.CKEditor5 || {}),
      (e.CKEditor5.mentionsIntegration = t()));
})(self, () =>
  (() => {
    "use strict";
    var e = {
      d: (t, i) => {
        for (var o in i) {
          e.o(i, o) &&
            !e.o(t, o) &&
            Object.defineProperty(t, o, {
              enumerable: !0,
              get: i[o],
            });
        }
        o: (e, t) => Object.prototype.hasOwnProperty.call(e, t);
        t = {};
      },
    };
    e.d(t, {
      default: () => i,
    });
    var active_editors = drupalSettings.ckeditor5Premium.active_editors;
    const i = {
      MentionsIntegration: class {
        constructor(e) {
          if (
            ((this.editor = e),
            void 0 === this.editor.plugins._availablePlugins ||
              !this.editor.plugins._availablePlugins.has("Mention") ||
              void 0 === drupalSettings.ckeditor5Premium ||
              void 0 === drupalSettings.ckeditor5Premium.mentions)
          ) {
            return;
          }

          var source_element_id = e.sourceElement.id;
          var active_editors = drupalSettings.ckeditor5Premium.active_editors;
          if (
            jQuery('select[data-editor-for="edit-body-0-value"]').length > 0
          ) {
            var selected_val = jQuery(
              'select[data-editor-for="edit-body-0-value"] option:selected'
            ).val();
            var mentions = [];

            if (
              drupalSettings.ckeditor5Premium.mentions.hasOwnProperty(
                selected_val
              )
            ) {
              drupalSettings.ckeditor5Premium.mentions[selected_val].forEach(
                function (item) {
                  var obj = {};
                  obj["feed"] = function (e) {
                    if (void 0 !== item) {
                      return new Promise((t) => {
                        jQuery.ajax(drupalSettings.annotations_url, {
                          data: {
                            query: e,
                            id: item.id,
                            limit: drupalSettings.dropdown_limit,
                          },
                          success: function (e) {
                            t(e);
                          },
                        });
                      });
                    }
                  };
                  obj["marker"] = item.marker;
                  obj["minimumCharacters"] = item.minimumCharacters;

                  mentions.push(obj);
                }
              );
            }
            if (jQuery.inArray(selected_val, active_editors) == -1) {
              mentions = [];
            }
            const t = {
              feeds: mentions,
              feeds1: [
                {
                  feed: this.getFeedItems,
                  marker: drupalSettings.ckeditor5Premium.mentions.marker,
                  minimumCharacters:
                    drupalSettings.ckeditor5Premium.mentions.minCharacter,
                  dropdownLimit:
                    drupalSettings.ckeditor5Premium.mentions.dropdownLimit,
                },
                {
                  feed: function (e) {
                    console.log("this is inline ", e);

                    if (
                      void 0 !== drupalSettings.ckeditor5Premium &&
                      void 0 !== drupalSettings.ckeditor5Premium.mentions
                    ) {
                      return new Promise((t) => {
                        jQuery.ajax(drupalSettings.annotations_url, {
                          data: {
                            query: e,
                          },
                          success: function (e) {
                            t(e);
                          },
                        });
                      });
                    }
                    marker: "@";
                    minimumCharacters: drupalSettings.ckeditor5Premium.mentions
                      .minCharacter;
                    dropdownLimit: drupalSettings.ckeditor5Premium.mentions
                      .dropdownLimit;
                  },
                },
              ],
            };
            (this.editor.config._config.mention = t),
              void 0 !== this.editor.config._config.comments &&
                void 0 !== this.editor.config._config.comments.editorConfig &&
                (this.editor.config._config.comments.editorConfig.extraPlugins.push(
                  this.editor.plugins._availablePlugins.get("Mention")
                ),
                (this.editor.config._config.comments.editorConfig.mention = t));
          }
        }
        static get pluginName() {
          return "MentionsIntegration";
        }
        getFeedItems(e) {
          console.log("this", this);
          if (
            void 0 !== drupalSettings.ckeditor5Premium &&
            void 0 !== drupalSettings.ckeditor5Premium.mentions
          ) {
            return new Promise((t) => {
              jQuery.ajax(drupalSettings.annotations_url, {
                data: {
                  query: e,
                },
                success: function (e) {
                  t(e);
                },
              });
            });
          }
        }
      },
    };
    return (t = t.default);
  })()
);
